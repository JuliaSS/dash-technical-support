export const state = () => ({
    TimeNumberCalls: [],
    statisticsUrl: [
        'number-of-calls-received',
    ],
  })
  export const mutations = {
    SET_TIME_NUMBER_CALLS(state, statistics) {
      state.TimeNumberCalls = statistics
    },
  }
  import axios from 'axios';
  export const actions = {
    async fetch({commit}, url) {
      const statisticsList = await axios.get(`https://xn--c1aj1aj.xn--p1ai/dashboard-service/api/v1/mdlp/${url}`)
        switch (url) {
            case 'number-of-calls-received':
            commit('SET_RECEIVED_CALLS', statisticsList.data.data)
            break;
            default:
            break;
        }
    },
    async fetchCall({commit},url){
          const statisticsList = await axios.get(`https://xn--c1aj1aj.xn--p1ai/dashboard-service/api/v1/mdlp/average-call-time/${url}`)
          commit('SET_TIME_NUMBER_CALLS', statisticsList.data.data)
    },
  }
  export const getters = {
    TIME_NUMBER_CALLS: s => s.TimeNumberCalls,
    STATISTICS_URL:s=>s.statisticsUrl,
  }